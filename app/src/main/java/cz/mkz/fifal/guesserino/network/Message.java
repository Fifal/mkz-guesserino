package cz.mkz.fifal.guesserino.network;

import java.util.Random;

/**
 * Created by fifal on 25.2.17.
 */

public class Message {
    private String text;
    private NetworkCode networkCode;
    private long hashCode;
    private boolean isConfirmed;
    private long sentTime;

    public Message(String text, NetworkCode networkCode) {
        this.text = text;
        this.networkCode = networkCode;
        this.hashCode = (text + networkCode.toString() + new Random().nextDouble()).hashCode();
        this.isConfirmed = false;
        this.sentTime = System.currentTimeMillis();
    }

    public long getHashCode() {
        return hashCode;
    }

    public boolean isConfirmed() {
        return isConfirmed;
    }

    public void confirm() {
        isConfirmed = true;
    }

    @Override
    public String toString() {
        if (text.equals("")) {
            return NetworkCode.PROTOCOL_PREFIX + networkCode.toString()+ ":" + hashCode + NetworkCode.PROTOCOL_SUFFIX;
        } else {
            return NetworkCode.PROTOCOL_PREFIX + networkCode.toString() + ":" + text + ":" + hashCode + NetworkCode.PROTOCOL_SUFFIX;
        }
    }

    public void updateSentTime(){
        this.sentTime = System.currentTimeMillis();
    }

    public long getSentTime(){
        return this.sentTime;
    }
}
