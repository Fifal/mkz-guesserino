package cz.mkz.fifal.guesserino.network;

/**
 * Created by fifal on 25.2.17.
 */

public enum NetworkCode {
    CL_CONNECT("cl_connect"), // Code for connecting to server
    CL_DISCONNECT("cl_disconnect"), // Disconnecting from server
    CL_IMAGE("cl_image"), // Sending image to server
    CL_GUESS("cl_guess"), // User is guessing
    CL_READY("cl_ready"), // User is ready
    CL_ACK("cl_ack:"),
    SRV_CONNECTED("srv_connected"), // Response from server, successfully logged
    SRV_NAME_TAKEN("srv_nickExists"),  // Name is already taken
    SRV_SERVER_FULL("srv_serverFull"), // Server is full
    SRV_IMAGE("srv_image"), // Server is sending image
    SRV_NOT_FOUND("srv_notFound"), // If server not responding
    SRV_DRAW("srv_draw"), // Client has to draw image
    SRV_WORD("srv_word"), // Word to draw
    SRV_BAD_NAME("srv_badName"),
    SRV_ACK("srv_ack"),
    SRV_BTNS_GUESS("srv_btnsGuess"),
    SRV_INFO("srv_info"),
    SRV_SCORE("srv_score"),
    SRV_DISCONNECT("srv_disconnect"),
    SRV_DROPPED("srv_dropped"),
    SRV_NOT_VALID("srv_notValidMessage"),
    NOT_NETWORK_CODE("not_network_code"); // Unknown network code

    private String networkCodeStr;
    public static String PROTOCOL_PREFIX = "ups:";
    public static String PROTOCOL_SUFFIX = ";";

    NetworkCode(String codeString) {
        this.networkCodeStr = codeString;
    }

    @Override
    public String toString(){
        return networkCodeStr;
    }

    public static NetworkCode getNetworkCode(String code) {
        if (code.equals(NetworkCode.SRV_ACK.toString())) {
            return NetworkCode.SRV_ACK;
        } else if (code.equals(NetworkCode.SRV_BAD_NAME.toString())) {
            return NetworkCode.SRV_BAD_NAME;
        } else if (code.equals(NetworkCode.SRV_BTNS_GUESS.toString())) {
            return NetworkCode.SRV_BTNS_GUESS;
        } else if (code.equals(NetworkCode.SRV_CONNECTED.toString())) {
            return NetworkCode.SRV_CONNECTED;
        } else if (code.equals(NetworkCode.SRV_DISCONNECT.toString())) {
            return NetworkCode.SRV_DISCONNECT;
        } else if (code.equals(NetworkCode.SRV_DRAW.toString())) {
            return NetworkCode.SRV_DRAW;
        } else if (code.equals(NetworkCode.SRV_DROPPED.toString())) {
            return NetworkCode.SRV_DROPPED;
        } else if (code.equals(NetworkCode.SRV_IMAGE.toString())) {
            return NetworkCode.SRV_IMAGE;
        } else if (code.equals(NetworkCode.SRV_INFO.toString())) {
            return NetworkCode.SRV_INFO;
        } else if (code.equals(NetworkCode.SRV_NAME_TAKEN.toString())) {
            return NetworkCode.SRV_NAME_TAKEN;
        } else if (code.equals(NetworkCode.SRV_NOT_FOUND.toString())) {
            return NetworkCode.SRV_NOT_FOUND;
        } else if (code.equals(NetworkCode.SRV_NOT_VALID.toString())) {
            return NetworkCode.SRV_NOT_VALID;
        } else if (code.equals(NetworkCode.SRV_SCORE.toString())) {
            return NetworkCode.SRV_SCORE;
        } else if (code.equals(NetworkCode.SRV_SERVER_FULL.toString())) {
            return NetworkCode.SRV_SERVER_FULL;
        } else if (code.equals(NetworkCode.SRV_WORD.toString())) {
            return NetworkCode.SRV_WORD;
        } else {
            return NetworkCode.NOT_NETWORK_CODE;
        }
    }
}
