package cz.mkz.fifal.guesserino;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import cz.mkz.fifal.guesserino.config.Config;
import cz.mkz.fifal.guesserino.network.Message;
import cz.mkz.fifal.guesserino.network.NetworkCode;
import cz.mkz.fifal.guesserino.network.TCP;
import cz.mkz.fifal.guesserino.utils.ImageUtils;

public class GameActivity extends Activity implements View.OnTouchListener {
    ImageView imageView;
    Bitmap bitmap;
    Canvas canvas;
    Paint paint;
    Path path = new Path();
    EditText textArea;
    TCP tcp;

    AlertDialog.Builder results;
    boolean resultsShown = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_game);
        Config.GAME_ACTIVITY = this;

        imageView = (ImageView) this.findViewById(R.id.imageView);
        imageView.setMaxWidth(Config.CANVAS_SIZE);
        imageView.setMaxHeight(Config.CANVAS_SIZE);
        imageView.setMinimumWidth(Config.CANVAS_SIZE);
        imageView.setMinimumHeight(Config.CANVAS_SIZE);

        bitmap = Bitmap.createBitmap(Config.CANVAS_SIZE, Config.CANVAS_SIZE, Bitmap.Config.ARGB_8888);
        canvas = new Canvas(bitmap);

        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStrokeWidth(5f);
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.ROUND);

        imageView.setImageBitmap(bitmap);
        imageView.setOnTouchListener(this);

        textArea = (EditText) findViewById(R.id.txt_serverLog);
        textArea.setVerticalScrollBarEnabled(true);

        setButtons(false);

        tcp = Config.TCP_CONNECTION;

        results = new AlertDialog.Builder(Config.GAME_ACTIVITY)
                .setTitle("Výsledky")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        resultsShown = false;
                        finishActivity();
                    }
                });
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        // Get the coordinates of the touch event
        float eventX = event.getX();
        float eventY = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                path = new Path();
                path.moveTo(eventX, eventY);
                return true;
            case MotionEvent.ACTION_MOVE:
                path.lineTo(eventX, eventY);
                break;
            default:
                return false;
        }

        canvas.drawPath(path, paint);

        // Makes our view repaint and call onDraw
        imageView.invalidate();
        return true;
    }

    /// ------------------------------------------------------

    public void clearCanvas() {
        clearCanvas(this.getCurrentFocus());
    }

    /**
     * Clears canvas back to white color
     *
     * @param v View
     */
    public void clearCanvas(View v) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Paint whitePaint = new Paint();
                whitePaint.setColor(Color.WHITE);
                canvas.drawRect(3, 0, canvas.getWidth() - 3, canvas.getHeight(), whitePaint);
                path.reset();
                imageView.setImageBitmap(bitmap);
                imageView.invalidate();
            }
        });
    }

    /**
     * Sets paint color to one of selected material design color
     *
     * @param v View
     */
    public void setPaintColor(View v) {
        switch (v.getId()) {
            case R.id.btn_clrBlack:
                paint.setColor(Color.BLACK);
                Toast.makeText(this, "Vybrána černá barva", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_clrPurple:
                paint.setColor(Color.argb(255, 170, 102, 204));
                Toast.makeText(this, "Vybrána fialová barva", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_clrBlue:
                paint.setColor(Color.argb(255, 51, 181, 229));
                Toast.makeText(this, "Vybrána modrá barva", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_clrGreen:
                paint.setColor(Color.argb(255, 153, 204, 0));
                Toast.makeText(this, "Vybrána zelená barva", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_clrYellow:
                paint.setColor(Color.argb(255, 255, 187, 51));
                Toast.makeText(this, "Vybrána žlutá barva", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_clrOrange:
                paint.setColor(Color.argb(255, 255, 136, 0));
                Toast.makeText(this, "Vybrána oranžová barva", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_clrRed:
                paint.setColor(Color.argb(255, 255, 68, 68));
                Toast.makeText(this, "Vybrána červená barva", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_clrWhite:
                paint.setColor(Color.WHITE);
                Toast.makeText(this, "Vybrána bílá barva", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    /**
     * Sends guessing word to a server
     *
     * @param v View
     */
    public void sendGuess(View v) {
        if (v.getId() == R.id.btn_guess) {
            EditText guessWord = (EditText) findViewById(R.id.txt_guess);
            String str = guessWord.getText().toString();
            str = str.replace(" ", "");

            Message message = new Message(str, NetworkCode.CL_GUESS);
            addMessage(message);
            guessWord.setText("");
        }
    }

    /**
     * Appends server log text area
     *
     * @param message message to append
     */
    public void appendServerLog(String message) {
        message = message.replace("<>", "\n");
        message = message.replace("><", ":");
        final String messageFinal = message;

        if(message.contains("## Výsledky hry ##")){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    results.setMessage(messageFinal).show();
                    resultsShown = true;
                    textArea.setText(messageFinal + "\n" + textArea.getText());

                }
            });

        }else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (textArea.getText().toString().equals("")) {
                        textArea.setText(messageFinal);
                    } else {
                        textArea.setText(messageFinal + "\n" + textArea.getText());
                    }
                }
            });
        }
    }

    /**
     * Sends image to a server
     *
     * @param v View
     */
    public void sendImage(View v) {
        byte[] imgArray = ImageUtils.getByteArrayFromImg(imageView.getDrawable());
        String imageStr = ImageUtils.getStringFromByteArray(imgArray);

        Message message = new Message(imageStr, NetworkCode.CL_IMAGE);
        addMessage(message);

        setButtons(false);
    }

    public void setImageViewImage(final Drawable drawable) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                imageView.setImageDrawable(drawable);
                imageView.invalidate();
            }
        });
    }

    public void setReady(View v) {
        if (v.getId() == R.id.btn_ready) {
            Button btn_ready = (Button) findViewById(R.id.btn_ready);
            btn_ready.setEnabled(false);

            Message msg = new Message("", NetworkCode.CL_READY);
            addMessage(msg);
        }
    }

    public void setWord(final String word) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView textView = (TextView) findViewById(R.id.txt_welcome);
                textView.setText(word);
            }
        });

    }

    public void setButtons(final boolean isDrawing) {
        final Button btn_send = (Button) findViewById(R.id.btn_send);
        final Button btn_erase = (Button) findViewById(R.id.btn_erase);
        final Button btn_guess = (Button) findViewById(R.id.btn_guess);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (isDrawing) {
                    btn_send.setEnabled(true);
                    btn_erase.setEnabled(true);
                    btn_guess.setEnabled(false);
                } else {
                    btn_send.setEnabled(false);
                    btn_erase.setEnabled(false);
                    btn_guess.setEnabled(false);
                }
            }
        });
    }

    public void enableGuessButton() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Button btn = (Button) findViewById(R.id.btn_guess);
                btn.setEnabled(true);
            }
        });
    }

    public void finishActivity() {
        if(!resultsShown) {
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
        Config.MAIN_ACTIVITY.invalidateOptionsMenu();
    }

    private void addMessage(final Message message) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                Config.MST_THREAD.addMessage(message);
            }
        };
        thread.start();
    }
}
