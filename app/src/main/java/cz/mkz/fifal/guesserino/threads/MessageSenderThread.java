package cz.mkz.fifal.guesserino.threads;

import android.util.Log;

import java.util.ArrayList;

import cz.mkz.fifal.guesserino.config.Config;
import cz.mkz.fifal.guesserino.network.Message;

import static java.lang.Thread.sleep;

/**
 * Created by fifal on 25.2.17.
 */

public class MessageSenderThread implements Runnable{
    private ArrayList<Message> messages;

    public MessageSenderThread(){
        this.messages = new ArrayList<Message>();
    }

    public void addMessage(Message message){
        if(Config.TCP_CONNECTION.getSocket() != null) {
            messages.add(message);
            Config.TCP_CONNECTION.sendMessage(message);
        }
    }

    public void ackMessage(long hashCode) {
        Log.i("[MST][ackMessage]", "Potvrzuji zprávu s hashem: " + hashCode);
        for (int i = 0; i < messages.size(); i++) {
            if (messages.get(i).getHashCode() == hashCode) {
                messages.get(i).confirm();
                Log.i("[MST][ackMessage]", "Zpráva potvrzena: " + messages.get(i).toString());
            }
        }
    }

    @Override
    public void run() {
        while (Config.MESSAGE_SENDER_RUNNING) {
            try {
                sleep(1);
            } catch (InterruptedException e) {
                Log.e("[MST][run]", e.getMessage());
            }

            for (int i = 0; i < messages.size(); i++) {
                if (!messages.get(i).isConfirmed() && (System.currentTimeMillis() > (messages.get(i).getSentTime() + 5000))) {
                    Config.TCP_CONNECTION.sendMessage(messages.get(i).toString());
                    try {
                        sleep(50);
                    } catch (InterruptedException e) {
                        Log.e("[MST][run]", e.getMessage());
                    }
                    messages.get(i).updateSentTime();
                }
            }
        }
    }
}
