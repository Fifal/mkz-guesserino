package cz.mkz.fifal.guesserino;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.io.IOException;

import cz.mkz.fifal.guesserino.config.Config;
import cz.mkz.fifal.guesserino.network.Message;
import cz.mkz.fifal.guesserino.network.NetworkCode;
import cz.mkz.fifal.guesserino.network.TCP;
import cz.mkz.fifal.guesserino.threads.ClientListenerThread;
import cz.mkz.fifal.guesserino.threads.MessageSenderThread;

public class MainActivity extends AppCompatActivity {
    Intent game;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        game = new Intent(this, GameActivity.class);

        //TODO: header color
        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.activity_main);

        setButtons(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_logout:
                if (Config.GAME_ACTIVITY != null) {
                    Config.CLIENT_LISTENER_RUNNING = false;
                    Config.MESSAGE_SENDER_RUNNING = false;
                    Config.GAME_ACTIVITY.finishActivity();

                    if (Config.TCP_CONNECTION.getSocket() != null) {
                        try {
                            Config.TCP_CONNECTION.getSocket().close();
                        } catch (IOException e) {
                            Log.e("[MA][OptionsItemSel]", "Chyba při zavírání socketu: " + e.getMessage());
                        }
                    }
                    showToast("Odpojení úspěšné!");
                    item.setEnabled(false);

                    Config.TCP_CONNECTION = null;
                    setButtons(false);
                }
                return true;
        }
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu (Menu menu) {
        if (Config.TCP_CONNECTION == null)
            menu.getItem(0).setEnabled(false);
        return true;
    }


    public void login(View v) {
        EditText ipAddressEt = (EditText) findViewById(R.id.txt_ip);
        EditText portEt = (EditText) findViewById(R.id.txt_port);
        EditText nickEt = (EditText) findViewById(R.id.txt_nick);

        int port = -1;
        try {
            port = Integer.parseInt(portEt.getText().toString());
        } catch (Exception ex) {
            Toast.makeText(this, "Špatný port!", Toast.LENGTH_SHORT).show();
            return;
        }

        if (ipAddressEt.getText().toString().equals("")) {
            Toast.makeText(this, "Prázdná IP adresa!", Toast.LENGTH_SHORT).show();
            return;
        }

        if (nickEt.getText().toString().equals("")) {
            Toast.makeText(this, "Prázdný nickname", Toast.LENGTH_LONG).show();
            return;
        }

        final String ip = ipAddressEt.getText().toString();
        final int portFinal = port;

        Thread thread = new Thread() {
            @Override
            public void run() {
                Config.TCP_CONNECTION = new TCP(ip, portFinal);
            }
        };
        thread.start();

        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (Config.TCP_CONNECTION.getSocket() == null) {
            Toast.makeText(this, "Chyba při připojování k serveru.", Toast.LENGTH_LONG).show();
        } else {
            Config.MESSAGE_SENDER_RUNNING = true;
            Config.CLIENT_LISTENER_RUNNING = true;
            Config.MAIN_ACTIVITY = this;

            ClientListenerThread clt = new ClientListenerThread();
            Thread clthread = new Thread(clt);
            clthread.start();

            MessageSenderThread mst = new MessageSenderThread();
            Config.MST_THREAD = mst;

            Thread msthread = new Thread(mst);
            msthread.start();

            final Message msg = new Message(nickEt.getText().toString(), NetworkCode.CL_CONNECT);

            Thread sendMsg = new Thread() {
                @Override
                public void run() {
                    Config.MST_THREAD.addMessage(msg);
                }
            };
            sendMsg.start();
        }
    }

    public void showGameUI() {
        startActivity(game);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
            setButtons(true);
            }
        });
    }

    public void showAlert(final String title, final String message, final int icon) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new AlertDialog.Builder(Config.MAIN_ACTIVITY)
                        .setTitle(title)
                        .setMessage(message)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setIcon(icon)
                        .show();
            }
        });
    }

    public void showToast(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(Config.MAIN_ACTIVITY, message, Toast.LENGTH_LONG).show();
            }
        });
    }

    public void backToGame(View v) {
        if (Config.TCP_CONNECTION != null && Config.GAME_ACTIVITY != null) {
            Intent intent = new Intent(this, GameActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        if (Config.TCP_CONNECTION != null && Config.GAME_ACTIVITY != null) {
            Intent intent = new Intent(this, GameActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(intent);
        }
    }

    public void setButtons(final boolean connected){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Button btn_backToGame = (Button) findViewById(R.id.btn_backToGame);
                Button btn_login = (Button) findViewById(R.id.btn_login);
                if(connected){
                    btn_backToGame.setEnabled(true);
                    btn_login.setEnabled(false);
                }else{
                    btn_backToGame.setEnabled(false);
                    btn_login.setEnabled(true);
                }
            }
        });
    }
}
